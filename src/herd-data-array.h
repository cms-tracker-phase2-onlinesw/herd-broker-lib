#ifndef HERD_DATA_ARRAY_H
#define HERD_DATA_ARRAY_H

#include "herd-data.h"
#include <string>
#include <iostream>
#include <sstream>

using namespace std;


template <typename T>
class HDataArray {
public:
///
/// Default constructor.
///
  HDataArray(int size);

  ///
  /// Default destructor.
  ///
  ~HDataArray();

  string setKey(int index, const string &key);
  string getKey(int index);
  T setData(int index, const T &data);
  T getData(int index);
  void print();
  string toYAML();
  string setRootNode(const string &rootNode);

private:
  HData<T>* _data;
  int _size;
  string _rootNode = "";
  void initData();
};

template <typename T>
HDataArray<T>::HDataArray(int size) {
  _size = size;
  _data = new HData<T>[_size];

  for (int i=0; i<_size; ++i) {
    stringstream ss;
    ss << i;
    _data[i].setKey(ss.str());
  }

  initData();
}

template <typename T>
HDataArray<T>::~HDataArray() {

}

template <typename T>
string HDataArray<T>::setRootNode(const string &rootNode) {
  return _rootNode = rootNode;
}

template <typename T>
string HDataArray<T>::setKey(int index, const string &key) {
  return _data[index].setKey(key);
}

template <typename T>
string HDataArray<T>::getKey(int index) {
  return _data[index].getKey();
}

template <typename T>
T HDataArray<T>::setData(int index, const T &data) {
  return _data[index].setData(data);
}

template <typename T>
T HDataArray<T>::getData(int index) {
  return _data[index].getData();
}

template <typename T>
void HDataArray<T>::print() {
  cout << toYAML() << endl;
}

template <typename T>
string HDataArray<T>::toYAML() {
  string data;
  string preamble = "";

  if (_rootNode != "") {
    data += _rootNode + ":\n";
    preamble = "\t";
  }

  for (int i=0; i<_size; ++i) {
    data += preamble + "- " + _data[i].toYAML();
    data += "\n";
  }

  return data;
}

template <typename T>
void HDataArray<T>::initData() {
  for (int i=0; i<_size; ++i) {
    _data[i].setData(0);
  }
}

template <>
void HDataArray<string>::initData() {
  for (int i=0; i<_size; ++i) {
    _data[i].setData("");
  }
}

#endif
