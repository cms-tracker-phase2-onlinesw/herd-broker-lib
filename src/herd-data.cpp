#include "herd-data.h"

template <typename T>
HerdData<T>::HerdData() {
  _key = "0";
  initData();
}

template <typename T>
HerdData<T>::HerdData(const string &key) {
  _key = key;
}

template <typename T>
HerdData<T>::HerdData(const string &key, const T &data) {
  _key = key;
  _data = data;
}

template <typename T>
HerdData<T>::HerdData(const string &key, const T *data, long size) {
  
}

template <typename T>
HerdData<T>::~HerdData() {
}

template <typename T>
string HerdData<T>::setKey(const string &key) {
  _key = key;
  return _key;
}

template <typename T>
string HerdData<T>::getKey() {
  return _key;
}

template <typename T>
T HerdData<T>::setData(const T &data) {
  _data = data;
  return _data;
}

template <typename T>
T HerdData<T>::getData() {
  return _data;
}

template <typename T>
void HerdData<T>::print() {
  cout << toYAML() << endl;
}

template <typename T>
string HerdData<T>::toYAML() {
  stringstream ss;
  string s;

  ss << _data;
  s = ss.str();
  return _key + ": " + s;
}

template <typename T>
void HerdData<T>::initData() {
  setData(0);
}

template <>
string HerdData<uint8_t>::toYAML() {
  stringstream ss;
  string s;

  ss << (int)_data;
  s = ss.str();
  return _key + ": " + s;
}

template <>
void HerdData<string>::initData() {
  setData("");
}

template class HerdData<std::string>;
