#include "herd-broker-header.h"

HerdBrokerHeader::HerdBrokerHeader() {

}

HerdBrokerHeader::HerdBrokerHeader(string DataType, int PartNumber, int TotalParts) {
  _DataType = DataType;
  _PartNumber = PartNumber;
  _TotalParts = TotalParts;
}

HerdBrokerHeader::HerdBrokerHeader(string yaml) {
  YAML::Node node = YAML::Node(yaml);
//  _DataType = node["DataType"];
//  _PartNumber = node["PartNumber"];
//  _TotalParts = node["TotalParts"];

}

HerdBrokerHeader::~HerdBrokerHeader() {

}

string HerdBrokerHeader::toYAML() {
  string yaml = "";

  // TODO: Convert this to using yaml-cpp library

  yaml += "DataType: " + _DataType + "\n";
  stringstream ssPartNum;
  ssPartNum << _PartNumber;
  yaml += "PartNumber: " + ssPartNum.str() + "\n";
  stringstream ssTotalParts;
  ssTotalParts << _TotalParts;
  yaml += "TotalParts: " + ssTotalParts.str() + "\n";

  return yaml;
}

int HerdBrokerHeader::TotalParts() {
  return _TotalParts;
}
