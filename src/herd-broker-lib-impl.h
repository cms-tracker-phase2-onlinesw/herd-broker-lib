template<class T>
bool HerdBrokerLib::GetMessage(HerdBrokerHeader &Header, HerdData<T> &Message) {
  int rc;
  zmq::message_t destination;
  zmq::message_t header;
  zmq::message_t message;
  zmq::message_t heartbeat;

  try {
        if ((rc = _heartBeatSocket->recv(&heartbeat, ZMQ_DONTWAIT)) == true) {
        string hbMsg = std::string(static_cast<char*>(heartbeat.data()), heartbeat.size());
        cout << "Heartbeat received:" << hbMsg << endl;

        s_send(*_heartBeatAckSocket, _heartBeatAckMsg);
      }
  }
  catch(zmq::error_t& e) {
    cout << "Error receiving heartbeat!" << endl;
  }

  // Part 1: Destination
  try {
    if ((rc = _inMsgSocket->recv(&destination, ZMQ_DONTWAIT)) != true) {
      return false;
    }
  }
  catch(zmq::error_t& e) {
    if (errno == EAGAIN)  // No pending messages
      return false;
  }

  // Part 2: Header
  _inMsgSocket->recv(&header);
  string HeaderStr = string(static_cast<char*>(header.data()), header.size());
  Header = HerdBrokerHeader(HeaderStr);

  // Part 3: Data
  _inMsgSocket->recv(&message);
  Message = std::string(static_cast<char*>(message.data()), message.size());

  return true;
}

template<class T>
bool HerdBrokerLib::SendMessage(HerdData<T> &Message) {
  try {
    // Part 1: Destination
    s_sendmore(*_outMsgSocket, SYSTEM_MESSAGE_ID);
    // Part 2: Header
    HerdBrokerHeader hbh(MSG_DATA_TYPE_STRING, 1, 1);
    s_sendmore(*_outMsgSocket, hbh.toYAML());
    // Part 3: Data
    string msg = Message.toYAML();
    s_send(*_outMsgSocket, msg);
  }
  catch(zmq::error_t& e) {
    std::cout << "Could not send system message!" << endl;
    return false;
  }

  return true;
}
