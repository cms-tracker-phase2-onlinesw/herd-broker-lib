#ifndef HERDBROKERHEADER_H
#define HERDBROKERHEADER_H

//#include "yaml-cpp/node/node.h"
#include <yaml-cpp/yaml.h>
#include <sstream>
#include <string>

using namespace std;


class HerdBrokerHeader {
public:
  HerdBrokerHeader();
  HerdBrokerHeader(string DataType, int PartNumber, int TotalParts);
  HerdBrokerHeader(string yaml);
  int TotalParts();
  ~HerdBrokerHeader();
  string toYAML();

private:
  string _DataType;
  int _TotalParts;
  int _PartNumber;

protected:

};

#endif
