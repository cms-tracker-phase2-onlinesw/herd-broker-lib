#ifndef HERD_DATA_H
#define HERD_DATA_H

#include <string>
#include <iostream>
#include <sstream>

using namespace std;


template <typename T>
class HerdData {
public:
  ///
  /// Default constructor.
  ///
  HerdData();
  ///
  /// \brief Constructor
  ///
  /// \param[in] key The key used to access the HerdData item.
  ///
  HerdData(const string &key);
  ///
  /// \brief Constructor
  ///
  /// \param[in] key The key used to access the HerdData item.
  /// \param[in] data The data stored in the HerdData item.
  ///
  HerdData(const string &key, const T &data);
  ///
  /// \brief Constructor
  ///
  /// \param[in] key The key used to access the HerdData item.
  /// \param[in] data The data stored in the HerdData item.
  /// \param[in] size The size of the data stored in the HerdData item.
  ///
  HerdData(const string &key, const T *data, long size);

  ///
  /// Default destructor.
  ///
  ~HerdData();

  ///
  /// \brief Sets the key for the HerdData item.
  ///
  /// \param[in] key The value of the key to be set.
  /// \return Returns the value of the key that was set.
  ///
  string setKey(const string &key);
  ///
  /// \brief Gets the key for the HerdData item.
  ///
  /// \return Returns the value of the key for the HerdData item.
  ///
  string getKey();
  ///
  /// \brief Sets the data value for the HerdData item.
  ///
  /// \param[in] dataq The value of the data to be set.
  /// \return Returns the value of the data that was set.
  ///
  T setData(const T &data);
  ///
  /// \brief Gets the value of the data for the HerdData item.
  ///
  /// \return Returns the value of the data for the HerdData item.
  ///
  T getData();
  ///
  /// \brief Pretty print for the HerdData item.
  ///
  /// Prints the key and data for the HerdData item to standard output.
  ///
  void print();
  ///
  /// \brief Converts the HerdData item to a YAML representation.
  ///
  /// \return Returns the YAML representation of the HerdData item.
  string toYAML();

private:
  string _key;
  T _data;
  void initData();
};


#endif
