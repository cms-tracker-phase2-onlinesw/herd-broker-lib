#include "herd-broker-lib.h"
#include "herd-broker-lib-impl.h"


HerdBrokerLib::HerdBrokerLib() {
  s_catch_signals();
  //_context = new zmq::context_t(1);
  //std::cout << "Created context." << std::endl;
  _messageSize = MSGSIZE;
}

HerdBrokerLib::~HerdBrokerLib() {
  delete _inMsgSocket;
  delete _outMsgSocket;
  delete _heartBeatSocket;
  delete _heartBeatAckSocket;
  delete _directMsgSocket;
  delete _context;
}

//void HerdBrokerLib::SetContext(zmq::context_t &context) {
//  _context = &context;
//}

bool HerdBrokerLib::Init(std::string ipAddress) {
  try {
    if (ipAddress == "")
      return false;

    _ipAddress = ipAddress;

    _context = new zmq::context_t();
    _inMsgSocket = new zmq::socket_t(*_context, ZMQ_SUB);
    _outMsgSocket = new zmq::socket_t(*_context, ZMQ_PUB);
    _heartBeatSocket = new zmq::socket_t(*_context, ZMQ_SUB);
    _heartBeatAckSocket = new zmq::socket_t(*_context, ZMQ_PUB);
    _directMsgSocket = new zmq::socket_t(*_context, ZMQ_SUB);
  }
  catch(zmq::error_t& e) {
    std::cout << "HERD-BROKER-LIB: Init Failed!" << endl;
    return false;
  }

  return true;
}

bool HerdBrokerLib::Connect(string ClientID) {
  int idLength;

  idLength = ClientID.length();

  if (ClientID == SYSTEM_MESSAGE_ID)
    return false;
  if (idLength != MESSAGE_ID_SIZE)
    return false;

  try {
    _inMsgSocket->connect(BROKER_MSG_PUB_ADDR);
    _outMsgSocket->connect(BROKER_MSG_SUB_ADDR);
    _heartBeatSocket->connect(BROKER_HEARTBT_ADDR);
    _heartBeatAckSocket->connect(BROKER_HRTBACK_ADDR);
    _directMsgSocket->bind(DIRBINDADDR());
  }
  catch(zmq::error_t& e) {
    std::cout << "HERD-BROKER-LIB: Could not connect ZMQ socket!" << endl;
    return false;
  }

  try {
    _inMsgSocket->setsockopt(ZMQ_SUBSCRIBE, WELCOME_MESSAGE_ID, MESSAGE_ID_SIZE);
    _inMsgSocket->setsockopt(ZMQ_SUBSCRIBE, SYSTEM_MESSAGE_ID, MESSAGE_ID_SIZE);
    _inMsgSocket->setsockopt(ZMQ_SUBSCRIBE, ClientID);
    _heartBeatSocket->setsockopt(ZMQ_SUBSCRIBE, HEARTBT_MESSAGE_ID, MESSAGE_ID_SIZE);
    _directMsgSocket->setsockopt(ZMQ_SUBSCRIBE, "", 0);
  }
  catch(zmq::error_t& e) {
    std::cout << "HERD-BROKER-LIB: Could not set ZMQ socket options!" << endl;
    return false;
  }

  s_sleep(100);

  _heartBeatAckMsg = ACKMSG(ClientID);
  _connected = true;

  return true;
}

bool HerdBrokerLib::Connected() {
  return _connected;
}

bool HerdBrokerLib::Disconnect() {

  if (_connected == false)
    return false;

  try {
    _inMsgSocket->disconnect(BROKER_MSG_PUB_ADDR);
    _outMsgSocket->disconnect(BROKER_MSG_SUB_ADDR);
    _heartBeatSocket->disconnect(BROKER_HEARTBT_ADDR);
    _heartBeatAckSocket->disconnect(BROKER_HRTBACK_ADDR);
    //_directMsgSocket->disconnect(DIRBINDADDR());
  }
  catch(zmq::error_t& e) {
    std::cout << "HERD-BROKER-LIB: Could not disconnect ZMQ socket!" << endl;
    return false;
  }

  _connected = false;
  return true;
}

long HerdBrokerLib::MessageSize() {
  return _messageSize;
}

void HerdBrokerLib::MessageSize(long bytes) {
  if (bytes > 0) {
    _messageSize = bytes;
  }
}

bool HerdBrokerLib::MesssageWaiting() {
  return false;
}

bool HerdBrokerLib::SendByteMessage(const string &DataType,
                                    const void *Data,
                                    const long DataSize) {
  if (DataSize < 1) {
    cout << "HERD-BROKER-LIB: SendByteMessage: DataSize must be greater than 0!" << endl;
    return false;
  }
  else if (DataSize > MAX_MESSAGE_BYTES) {
    cout << "HERD-BROKER-LIB: SendByteMessage: DataSize must be less than MAX_MESSAGE_BYTES!" << endl;
    return false;
  }

  bool done = false;
  long msgSize;
  long dataStart = 0;
  long numParts = (DataSize / _messageSize) + (DataSize % _messageSize != 0);

  for (long i=0; i<numParts; ++i) {
    if (i < (numParts-1)) {
      msgSize = _messageSize;
    }
    else {
      msgSize = DataSize - (_messageSize*(numParts-1));
    }

    dataStart = _messageSize * i;

    try {
      // Part 1: Desitination
      s_sendmore(*_outMsgSocket, SYSTEM_MESSAGE_ID);
      // Part 2: Header
      HerdBrokerHeader hbh(DataType, i, numParts);
      s_sendmore(*_outMsgSocket, hbh.toYAML());
      // Part 3: Data
      zmq::message_t message(msgSize);
      memcpy(message.data(), ((char*)Data)+(i*_messageSize), msgSize);
      cout << "HERD-BROKER-LIB: Sending Message" << endl; //<< message.data() << endl;
      _outMsgSocket->send(message);
    }
    catch(zmq::error_t& e) {
      std::cout << "HERD-BROKER-LIB: Could not send system message!" << endl;
      return false;
    }
  }

  return true;
}

bool HerdBrokerLib::GetHeartbeat() {
  int rc;
  zmq::message_t heartbeat;

  try {
      if ((rc = _heartBeatSocket->recv(&heartbeat, ZMQ_DONTWAIT)) == true) {
        string hbMsg = std::string(static_cast<char*>(heartbeat.data()), heartbeat.size());
        cout << "HERD-BROKER-LIB: Heartbeat Received" << endl; //<< hbMsg << endl;

        s_send(*_heartBeatAckSocket, _heartBeatAckMsg);
      }
  }
  catch(zmq::error_t& e) {
    cout << "HERD-BROKER-LIB: Error receiving heartbeat!" << endl;
    return false;
  }

  return true;
}

bool HerdBrokerLib::GetByteMessage(void *Data, long &DataSize) {
  int rc;
  zmq::message_t destination;
  zmq::message_t header;
  string headerYAML;

  DataSize = 0;

  cout << "START GETBYTEMESSAGE" << endl;

  try {
    // Destination
    if ((rc = _inMsgSocket->recv(&destination, ZMQ_DONTWAIT)) != true) {
      return false;
    }
  }
  catch(zmq::error_t& e) {
    if (errno == EAGAIN)  // No pending messages
      return false;
  }

  try {
    _inMsgSocket->recv(&header);
  }
  catch(zmq::error_t& e) {
    return false;
  }

  headerYAML = std::string(static_cast<char*>(header.data()), header.size());
  HerdBrokerHeader hbh(headerYAML);
  int numParts = hbh.TotalParts();

  cout << "Header:" << headerYAML << endl;
  cout << "numParts: " << numParts << endl;

  for (int i=0; i<numParts; ++i) {
    zmq::message_t message;

    try {
      _inMsgSocket->recv(&message);
    }
    catch(zmq::error_t& e) {
      return false;
    }

    cout << "HERD-BROKER-LIB: Received Message" << endl; // << message.data() << endl;
    memcpy((char*)Data+(i*_messageSize), message.data(), message.size());
    DataSize += message.size();

    if (i<numParts) { // Get Next Header
      zmq::message_t header;

      try {
        _inMsgSocket->recv(&header);
      }
      catch(zmq::error_t& e) {
        return false;
      }
    }
  }

  cout << "END GETBYTEMESSAGE" << endl;

  return true;
}

bool HerdBrokerLib::RequestDirecConnectionAddress(const string &ClientID) {
  HerdData<string> hd(REQADD(ClientID));
  return SendMessage(hd);
}

bool HerdBrokerLib::OpenDirectConnection(const string &ClientID) {
  return false;
}

bool HerdBrokerLib::CloseDirectConnection(const string &ClientID) {
  return false;
}

bool HerdBrokerLib::SendDirectMessage(const string &ClientID,
                                      const string &Message) {
  try {
    s_sendmore(*_outMsgSocket, ClientID);
    s_send(*_outMsgSocket, Message);
  }
  catch(zmq::error_t& e) {
    std::cout << "HERD-BROKER-LIB: Could not send direct message!" << endl;
    return false;
  }

  return true;

}

bool HerdBrokerLib::SendByteDirectMessage(const string &ClientID,
                                          const string &MetaData,
                                          const unsigned char *Data) {
  return false;
}

bool HerdBrokerLib::Register() {
  try {
    HerdData<string> hd(REGMSG(_clientID, DIRADDR(_ipAddress)));
    SendMessage(hd);
  }
  catch(int e) {
    return false;
  }

  return true;
}
