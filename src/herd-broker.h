#ifndef HERDBROKER_H
#define HERDBROKER_H


#define BROKER_MSG_PUB_ADDR "tcp://172.19.0.10:5560"
#define BROKER_MSG_SUB_ADDR "tcp://172.19.0.10:5559"
#define BROKER_HEARTBT_ADDR "tcp://172.19.0.10:5561"
#define BROKER_HRTBACK_ADDR "tcp://172.19.0.10:5562"
#define APPLICATION_SUB_PORT "5563"


#define MESSAGE_ID_SIZE   4     /// The size of the message ID header.
#define SYSTEM_MESSAGE_ID "SYSM" /// ID of messages meant for all clients. Each client should subscribe to this message ID.
#define WELCOME_MESSAGE_ID  "CONN"  /// Welcome message sent from Broker to Client on connect.
#define HEARTBT_MESSAGE_ID  "PING"  /// Heartbeat message sent from Broker to Client.

#define MSGSIZE 2;  /// Default max size of a message packet payload
#define MAX_MESSAGE_BYTES 1500  /// Max size of any message

#define MSG_DATA_TYPE_STRING  "string"

#endif
