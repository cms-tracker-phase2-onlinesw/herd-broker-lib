#ifndef HERDBROKERLIB_H
#define HERDBROKERLIB_H

#include "herd-broker.h"
#include "herd-broker-header.h"
#include "herd-data.h"
#include <string>
#include "zhelpers.hpp"

#define ACKMSG(m) "ACK" + m
#define ACKCID(m) m.substr(std::max<int>(m.size()-4,0))
#define REGMSG(client, addr) std::string("REG" + client + addr)
#define REQADD(client) std::string("REQ" + client)
#define DIRADDR(ip) "tcp://" + ip + ":" + APPLICATION_SUB_PORT
#define DIRBINDADDR() std::string("tcp://*:") + APPLICATION_SUB_PORT

using namespace std;


class HerdBrokerLib {
public:
  HerdBrokerLib();
  ~HerdBrokerLib();

  //void SetContext(zmq::context_t &context);
  bool Init(std::string ipAddress);
  bool Connect(string ClientID);
  bool Connected();
  bool Disconnect();
  long MessageSize();
  void MessageSize(long bytes);
  bool MesssageWaiting();
  template<class T>
  bool GetMessage(HerdBrokerHeader &Header, HerdData<T> &Message);
  template<class T>
  bool SendMessage(HerdData<T> &Message);
  bool SendByteMessage(const string &DataType, const void *Data,
                       const long DataSize);
  bool GetHeartbeat();
  bool GetByteMessage(void *Data, long &DataSize);
  bool SendDirectMessage(const string &ClientID, const string &Message);
  bool SendByteDirectMessage(const string &ClientID, const string &DataType,
                             const unsigned char *Data);
  bool Register();
  bool OpenDirectConnection(const string &ClientID);
  bool CloseDirectConnection(const string &ClientID);

private:
  string _clientID; /// The client ID for this application. Must be unique for each application.
  bool _connected = false;  /// Indicates if this client is currently connected.
  zmq::context_t* _context = NULL; /// ZMQ Context for the application.
  zmq::socket_t* _inMsgSocket = NULL;   /// ZMQ Socket to read data from applications.
  zmq::socket_t* _outMsgSocket = NULL;  /// ZMQ Socket used to foward data to applications.
  zmq::socket_t* _heartBeatSocket = NULL;  /// ZMQ Socket used to receive Heartbeat messages from Broker.
  zmq::socket_t* _heartBeatAckSocket = NULL; /// ZMQ Socket that clients will use to respond to Heartbeats (so the server knows that they are still alive).
  zmq::socket_t* _directMsgSocket = NULL; /// ZMQ Socket that applications can send messages directly to.
  std::string _ipAddress; /// IP Address of client application.
  bool _lastHeartbeat;
  string _heartBeatAckMsg;
  long _messageSize;

  bool RequestDirecConnectionAddress(const string &ClientID);

protected:

};

#endif
