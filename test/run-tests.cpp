#include "herd-broker-lib-test.cpp"
#include "herd-broker-header-test.cpp"
#include <iostream>

using namespace std;


int main(int argc, char **argv) {
  cerr << "**********************************************\n";
  cerr << "* NOTE: Make sure that the broker is running *\n";
  cerr << "* before executing these tests!              *\n";
  cerr << "**********************************************\n";
  cerr << "\n\n";

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
