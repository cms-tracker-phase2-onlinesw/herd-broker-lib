#include "../src/herd-broker-lib.h"
#include "../src/herd-broker-lib-impl.h"
#include "gtest/gtest.h"

#define HOST_IP "172.19.0.11"

//TEST (Broker, BrokerIsRunning) {
//
//}

TEST (Initialization, IPAddressNotNull) {
  HerdBrokerLib hb;
  EXPECT_EQ(false, hb.Init(""));
}

TEST (ConnectionTest, ClientIDNotNull) {
  HerdBrokerLib hb;
  hb.Init(HOST_IP);

  EXPECT_EQ(false, hb.Connect(""));
  hb.Disconnect();
}

TEST (ConnectionTest, ClientIDLength) {
  HerdBrokerLib hb;
  hb.Init(HOST_IP);

  //Test ClientID length < 4
  EXPECT_EQ(false, hb.Connect("X"));
  //Test ClientID length > 4
  EXPECT_EQ(false, hb.Connect("XXXXX"));
  //Test ClientID length == 4
  EXPECT_EQ(true, hb.Connect("XXXX"));
  hb.Disconnect();
}

TEST (ConnectionTest, ClientIDNotSYSM) {
  HerdBrokerLib hb;
  hb.Init(HOST_IP);

  EXPECT_EQ(false, hb.Connect(SYSTEM_MESSAGE_ID));
  hb.Disconnect();
}

TEST (ConnectionTest, Connect) {
  HerdBrokerLib hb;
  hb.Init(HOST_IP);

  EXPECT_EQ(true, hb.Connect("1234"));
  hb.Disconnect();
}

TEST(ConnectionTest, IsConnected) {
  HerdBrokerLib hb;
  hb.Init(HOST_IP);

  EXPECT_EQ(false, hb.Connected());
  hb.Connect("1234");
  EXPECT_EQ(true, hb.Connected());
  hb.Disconnect();
  EXPECT_EQ(false, hb.Connected());
}

TEST (ConnectionTest, Disconnect) {
  HerdBrokerLib hb;
  hb.Init(HOST_IP);

  EXPECT_EQ(false, hb.Disconnect());
  hb.Connect("1234");
  EXPECT_EQ(true, hb.Disconnect());
}

TEST (SendMessageTest, SendSystemMessage) {
  HerdBrokerLib hb;
  string destID = "1234";
  HerdData<string> message("Testing SendMessage()...");

  hb.Init(HOST_IP);

  hb.Connect(destID);
  EXPECT_EQ(true, hb.SendMessage(message));
  hb.Disconnect();
}
/*
TEST (SendMessageTest, SendOversizedByteSystemMessage) {
    HerdBrokerLib hb;
    string destID = "1234";
    string metaData = "Metadata about the data...";
    unsigned char byteData[MAX_MESSAGE_BYTES+1];

    hb.Init(HOST_IP);

    hb.Connect(destID);
    EXPECT_EQ(false, hb.SendByteMessage(metaData, (void*)byteData, MAX_MESSAGE_BYTES+1));
    hb.Disconnect();
}
*/

TEST (SendMessageTest, SendByteMessage) {
    HerdBrokerLib hb;
    HerdBrokerHeader hbhIn;
    //string message;
    string destID = "1234";
    unsigned char byteData[5] = {65,65,65,65,65};
    unsigned char byteRecv[5];
    long recvBytes;
    int retries = 0;

    hb.Init(HOST_IP);

    hb.Connect(destID);
    s_sleep(1000);

    EXPECT_EQ(true, hb.SendByteMessage("unsigned char", (void*)byteData, 5));
    s_sleep(500);

    while (!hb.GetByteMessage((void*)byteRecv, recvBytes) && (retries++ < 5)) {
      std::cout << "Waiting for response...\n";
      s_sleep(500);
    }

    EXPECT_EQ(byteData[0], byteRecv[0]);
    EXPECT_EQ(byteData[4], byteRecv[4]);
    hb.Disconnect();
}

TEST (ReceiveMessageTest, ReceiveSystemMessage) {
  HerdBrokerLib hb;
  HerdBrokerHeader hbh;
  string destID = "1234";
  HerdData<string> testMessage("Testing GetMessage()...");
  HerdData<string> message;
  int retries = 0;

  hb.Init(HOST_IP);

  hb.Connect(destID);
  s_sleep(1000);
  hb.SendMessage(testMessage);
  s_sleep(1000);
  //EXPECT_EQ(true, hb.GetMessage(hbh, message));
  while (!hb.GetMessage(hbh, message) && (retries++ < 5)) {
    std::cerr << "Waiting for response...\n";
    s_sleep(500);
  }
  EXPECT_EQ(testMessage.toYAML(), message.toYAML());
  std::cout << "Received: \n" << message.toYAML() << endl;
  hb.Disconnect();
}

TEST (RegisterClient, SendMessageToBroker) {
  HerdBrokerLib hb;
  hb.Init(HOST_IP);

  EXPECT_EQ(true, hb.Register());
}

/* TEST (RegisterClient, GetAddressFromBroker) {
  HerdBrokerLib hb;
  hb.Init(HOST_IP);

  hb.Register();
  s_sleep(100); // Make sure that the broker processed our command

  EXPECT_EQ("tcp://172.19.0.11:5563", hb.Lookup);
} */
