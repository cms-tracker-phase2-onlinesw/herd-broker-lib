#include "../src/herd-broker-header.h"
#include "gtest/gtest.h"

#define HOST_IP "172.19.0.11"

TEST (Initialization, CreatesYaml) {
  HerdBrokerHeader hbh("DataType", 1, 3);
  EXPECT_NE("", hbh.toYAML());
}
